'use strict';

let getHookByName = async (ssClient, hookName) => {
    let hook;
    try {
        let hooksRes = await ssClient.webhooks.listWebhooks({});
        if (hooksRes && hooksRes.data) {
            let hooks = hooksRes.data;
            hook = hooks.find(obj => obj.name === hookName);
        }
    } catch (e) { return e; }
    return hook;
};

let listWebhooks = async (ssClient) => {
    let hooks;
    try {
        let hooksRes = await ssClient.webhooks.listWebhooks({});
        hooks = hooksRes.data
    } catch (e) { return e; }
    return hooks;
};

let cleanWebhooks = async (ssClient) => {
    let deletedRes = [];
    try {
        let hooksRes = await ssClient.webhooks.listWebhooks({});
        if (hooksRes.totalCount > 0) {
            for (let hook of hooksRes.data) {
                let res = await ssClient.webhooks.deleteWebhook({webhookId: hook.id});
                deletedRes.push(res);
            }
        }
    } catch (e) { return e; }
    return deletedRes;
};

let enableWebhook = async (ssClient, hook) => {
    let opts = {
        webhookId: hook.id,
        callbackUrl: hook.callbackUrl,
        body: { enabled: true }
    };
    let updateRes = await ssClient.webhooks.updateWebhook(opts);
    return updateRes;
};

let reloadWebhooks = async (ssClient, hooksOpts) => {
    let newHooks = [];
    let extraOpts = {
        scope: 'sheet',
        events: ['*.*'],
        version: 1
    };
    try {
        await cleanWebhooks(ssClient);
        for (let hookOpts of hooksOpts) {
            let opts = Object.assign(hookOpts, extraOpts);
            let hookRes = await ssClient.webhooks.createWebhook({body: opts});
            hookRes = await enableWebhook(ssClient, hookRes.result);
            newHooks.push(hookRes);
        }
    } catch (e) { return e; }
    return newHooks;
};

module.exports = {
    reloadWebhooks: reloadWebhooks,
    getWebhookByName: getHookByName,
    listWebhooks: listWebhooks,
    cleanWebhooks: cleanWebhooks
};