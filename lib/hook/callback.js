'use strict';

/// callback response body.events example:
//{"nonce":"899e6c8e-ba7c-4fe3-a509-dd41dcc34f47",
// "timestamp":"2020-09-23T22:55:07.082+0000",
// "webhookId":6447490519066500,
// "scope":"sheet",
// "scopeObjectId":6265599174174596,
// "events":[{"objectType":"sheet","eventType":"updated","id":6265599174174596,"userId":1484608161245060,"timestamp":"2020-09-23T22:55:01.000+0000"},
// {"objectType":"row","eventType":"updated","id":5534682916644740,"userId":1484608161245060,"timestamp":"2020-09-23T22:55:01.000+0000"},
// {"objectType":"cell","eventType":"created","rowId":5534682916644740,"columnId":4049762784700292,"userId":1484608161245060,"timestamp":"2020-09-23T22:55:01.000+0000"},
// {"objectType":"cell","eventType":"created","rowId":5534682916644740,"columnId":8553362412070788,"userId":1484608161245060,"timestamp":"2020-09-23T22:55:01.000+0000"},
// {"objectType":"cell","eventType":"created","rowId":5534682916644740,"columnId":390588087461764,"userId":1484608161245060,"timestamp":"2020-09-23T22:55:01.000+0000"},
// {"objectType":"cell","eventType":"updated","rowId":5534682916644740,"columnId":7427462505228164,"userId":1484608161245060,"timestamp":"2020-09-23T22:55:01.000+0000"}]}

let processBody = (body) => {
    let res = {};
    if (body.challenge) { /// for validation
        // received verification callback
        // verify we are listening by echoing challenge value
        res = {smartsheetHookResponse: body.challenge};
    } else if (body.events) { /// for actual sheet change events
        res = body.events;
    } else if (body.newWebHookStatus) { /// for status change
        res = {message: `Received status callback, new status: ${body.newWebHookStatus}`};
    } else {
        res = {message: `Received unknown callback: ${body}`};
    }
    return res;
};

module.exports = {
    processBody: processBody
};