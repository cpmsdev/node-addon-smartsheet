'use strict';

let rowdata = require('../row/rowdata');
let kvPairs = require('../utils/keyvalPairs');

let insertData = async (ssClient, sheetId, dataArray) => {
    let loadedRes = null;
    try {
        loadedRes = rowdata.insertRows(ssClient, sheetId, dataArray);
    } catch (e) { return e; }
    return loadedRes;
};

let deleteAllData = async (ssClient, sheetId) => {
    let deletedRes = null;
    try {
        let thisSheet = await ssClient.sheets.getSheet({id: sheetId});
        let rowIds = thisSheet.rows.map(obj => obj.id) || [];
        while (rowIds.length > 200) {
            let delIds = rowIds.splice(0, 200);
            deletedRes = await rowdata.deleteRows(ssClient, sheetId, delIds);
        }
        deletedRes = await rowdata.deleteRows(ssClient, sheetId, rowIds);
    } catch (e) {
        return e;
    }
    return deletedRes;
};

/// convert all data into key:value JSON format
let parseRows = (rows = [], colIdNamePairs = []) => {
    let data = [];
    for (let row of rows) {
        data.push(rowdata.parseCells2Data(row, colIdNamePairs));
    }
    return data || [];
};

let parseAllData = async (ssClient, sheetId) => {
    let data = [], colNameIdPairs;
    let params = {
        pageSize: 500, /// default is 100, it can be at most 500
        page: 1
    };
    try {
        while (params.page <= 40) { /// a limit of 20,000 rows on a sheet
            let thisSheet = await ssClient.sheets.getSheet({
                id: sheetId,
                queryParameters: params});
            if (!colNameIdPairs) {
                colNameIdPairs = thisSheet.columns
                    && kvPairs.genKeyValPairs(thisSheet.columns, 'title', 'id');
            }
            data.push(...parseRows(thisSheet.rows, colNameIdPairs));
            if(data.length >= thisSheet.totalRowCount) break;
            params.page ++;
        }

    } catch (e) { return e; }
    return data;
};

let logComment = async (ssClient, sheetId, text) => {
    let commentRes;
    try {
        let comment = {
            "comment": {
                "text": `[CPMS System Comments] ${text || ''}`
            }
        };
        commentRes = ssClient.sheets.createDiscussion({
            sheetId: sheetId, body: comment });
    } catch (e) { return e; }
    return commentRes;
};

module.exports = {
    insertData: insertData,
    deleteAllData: deleteAllData,
    parseAllData: parseAllData,
    log: logComment
};