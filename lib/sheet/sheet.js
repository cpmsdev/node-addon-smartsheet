'use strict';

/**
 * Get sheet object by its name
 * @param {*} ssClient Smartsheet client
 * @param {string} sheetName Name of the sheet to get
 * @returns {Promise<undefined | SSSheet | SSError>} A promise that resolves to undefined if the
 * sheet cannot be found, otherwise resolves to the {@link SSSheet} requested or to a generic {@link SSError}.
 */
let getSheetByName = async (ssClient, sheetName) => {
    /** @type SSSheet */
    let thisSheet = {};
    try {
        /** @type (ListSheetResponse | SSError) */
        let res = await ssClient.sheets.listSheets();
        let totalPgs = res.totalPages;
        thisSheet = res.data.find(obj => obj.name === sheetName);
        for (let p = 2; p <= totalPgs; p ++) {
            res = await ssClient.sheets.listSheets({
                queryParameters: { page: p }
            });
            thisSheet = res.data.find(obj => obj.name === sheetName);
            if (thisSheet) { break; }
        }
    } catch (e) { return e; }
    return thisSheet;
};

/**
 * Delete a sheet permanently
 * @param {*} ssClient Smartsheet client
 * @param {number} sheetId Sheet Id to delete
 * @returns {Promise<DeleteSheetResponse | SSError>} A promise that resolves to either {@link DeleteSheetResponse}
 * or to a generic {@link SSError}.
 */
let deleteSheet = async (ssClient, sheetId) => {
    /** @type (DeleteSheetResponse | SSError) */
    let deleted;
    try {
        deleted = await ssClient.sheets.deleteSheet({id: sheetId});
    } catch (e) { return e; }
    return deleted;
};

/**
 * Create a new sheet
 * @param {*} ssClient Smartsheet client
 * @param {string} sheetName Name of new sheet
 * @param {SSColumn[]} columns Column objects to add to new sheet
 * @param {number} [workspaceId] Workspace Id to create new sheet in. If blank, creates sheet in "Sheets" folder.
 * @returns {Promise<false | SSSheet | SSError>} A promise that resolves to false if partial success, otherwise
 * resolves to the newly created {@link SSSheet} or to a generic {@link SSError}.
 */
let createSheet = async (ssClient, sheetName, columns, workspaceId) => {
    /** @type (false | SSSheet) */
    let newSheet;
    let sheetOpts = {
        name: sheetName,
        columns: columns
    };
    try {
        let opts = { body: sheetOpts };
        /** @type (CreateSheetResponse | SSError) */
        let res;
        if (workspaceId) {
            opts['workspaceId'] = workspaceId;
            res = await ssClient.sheets.createSheetInWorkspace(opts);
        } else {
            res = await ssClient.sheets.createSheet(opts);
        }
        newSheet = (res.message === 'SUCCESS') && res.result;
    } catch (e) { return e; }
    return newSheet;
};

module.exports = {
    createSheet: createSheet,
    getSheetByName: getSheetByName,
    deleteSheet: deleteSheet
};