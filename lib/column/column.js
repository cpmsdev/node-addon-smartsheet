'use strict';

let getColType = (val) => {
    if ((typeof val !== 'number') && !isNaN(Date.parse(val))) { return 'DATE'; }
    return 'TEXT_NUMBER';
};

/// dataArray sample ///
// [{"PRJ_NAME":"PARK: Hathaway Park Renovation",
// "PRJ_ID":8113,
// "IMPL_MGR":"Mastrodicasa, Chris",
// "PRJ_PHASE":"Phase V - Construction",
// "ARCHIVED":0,
// "SWPPP":0,
// "C3":0,
// "CONTRACTOR":"Agbayani Construction Corporation",
// "LAST_UPDATE":"2020-05-07T16:53:00.000Z",
// "CURR_EXPENDITURE":1168793.03,
// "CURRENT_TOTAL_BUDGET":1177105,
// "CURR_RM_BALANCE":8311.97},
// ...]

let defineColumnsFromData = (dataArray=[], primaryKey) => {
    if (dataArray.length === 0) { return []; }
    let columns = [];
    let sampleData = dataArray[0];
    for (let [key, val] of Object.entries(sampleData)) {
        let col = {
            title: key,
            type: getColType(val)
        };
        if (key === primaryKey) {
            col['primary'] = true;
        }
        columns.push(col);
    }
    return columns;
};

/// get an entire column's values, and return an array of strings
let getColumnData = async (ssClient, sheetId, columnId) => {
    let dataArray = [];
    try {
        let thisSheet = await ssClient.sheets.getSheet({id: sheetId});
        let rows = thisSheet.rows || [];
        for (let row of rows) {
            let cell = row.cells.find(obj => obj.columnId === columnId);
            if (cell.value) {
                dataArray.push(cell.value);
            }
        }
    } catch (e) { return e; }

    return dataArray;
};

let getColumnByName = async (ssClient, sheetId, columnName) => {
    let column;
    try {
        let columns = await ssClient.sheets.getColumns({sheetId: sheetId});
        column = columns.data.find(obj => obj.title === columnName);
    } catch (e) { return e; }
    return column;
}

module.exports = {
    defColsFromData: defineColumnsFromData,
    getColumnData: getColumnData,
    getColumnByName: getColumnByName
};