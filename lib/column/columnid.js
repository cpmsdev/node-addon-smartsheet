'use strict';

let kvPairs = require('../utils/keyvalPairs');

let listColumnIds = async (ssClient, sheetId) => {
    let opts = {
        sheetId: sheetId
    };
    try {
        let columns = await ssClient.sheets.getColumns(opts);
        let colNameIdPairs = columns.data
            && kvPairs.genKeyValPairs(columns.data, 'title', 'id');
        return colNameIdPairs;
    } catch (e) {
        // sample e
        // {
        //     statusCode: 401,
        //     errorCode: 1002,
        //     message: "Your Access Token is invalid."
        // }
        return e;
    }
};

module.exports = {
    listColumnIds: listColumnIds
};


