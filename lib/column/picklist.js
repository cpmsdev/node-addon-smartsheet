'use strict';

let column = require('./column');
let sheetData = require('../sheet/sheetdata');

/// input parameters ///
/// sourColumn, destColumn:
/// {sheetId: ***, columnId: ***}
let syncPicklist = async (ssClient, sourColumn, destColumn) => {
   let res;
   try {
      let sourceData = await column.getColumnData(ssClient, sourColumn.sheetId, sourColumn.columnId);
      let opts = {
         sheetId: destColumn.sheetId,
         columnId: destColumn.columnId,
         body: {
            type: 'PICKLIST',
            options: sourceData
         }
      };
      res = await ssClient.sheets.updateColumn(opts);
   } catch (e) { return e; }
   return res;
};

let updatePicklist = async (ssClient, dataArray, destColumn) => {
   let res;
   try {
      let opts = {
         sheetId: destColumn.sheetId,
         columnId: destColumn.columnId,
         body: {
            type: 'PICKLIST',
            options: dataArray
         }
      };
      res = await ssClient.sheets.updateColumn(opts);
   } catch (e) { return e; }
   return res;
};

module.exports = {
   syncPicklist: syncPicklist,
   updatePicklist: updatePicklist
};