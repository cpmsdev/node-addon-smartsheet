'use strict';

let kvPairs = require('../utils/keyvalPairs');

/// cellObj example
// {
//     "columnId": 8591165092063108,
//     "virtualColumnId": 6330262632064900,
//     "value": 9611,
//     "displayValue": "9611" (optional)
// }
let parseCells2Data = (row, colIdNamePairs) => {
    let cellsData = {};
    for (let cellObj of row.cells) {
        let key = Object.keys(colIdNamePairs)
            .find(key => colIdNamePairs[key] === cellObj.virtualColumnId);

        if (cellObj.hyperlink && cellObj.hyperlink.url) { // extract url if cell has a hyperlink
            let title = cellObj.value;

            // give it a generic 'Link' title if none was set in smartsheet
            if (title === cellObj.hyperlink.url) {
                title = 'Link';
            }

            cellsData[key] = { title: title, url: cellObj.hyperlink.url };
        } else {
            cellsData[key] = cellObj.value;
        }
    }
    return cellsData;
};

let parseRows = (rows = [], colIdNamePairs = []) => {
    let data = [];
    for (let row of rows) {
        data.push(parseCells2Data(row, colIdNamePairs));
    }
    return data || [];
};

let parseReportData = async (ssClient, reportId) => {
    let data = [], colNameIdPairs;
    let params = {
        pageSize: 500, /// maximum of 10,000 rows per request
        page: 1
    }
    try {
        while (params.page <= 40) { /// report limit is 50,000 rows
            let thisRpt = await ssClient.reports.getReport({
                id: reportId,
                queryParameters: params});
            if (!colNameIdPairs) {
                colNameIdPairs = thisRpt.columns
                    && kvPairs.genKeyValPairs(thisRpt.columns, 'title', 'virtualId');
            }
            data.push(...parseRows(thisRpt.rows, colNameIdPairs));
            if (data.length >= thisRpt.totalRowCount) break; /// reach the last page
            params.page ++;
        }

    } catch (e) { return e; }
    return data;
};

module.exports = {
    parseReportData: parseReportData
};

