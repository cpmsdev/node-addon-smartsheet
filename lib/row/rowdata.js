'use strict';

let columnid = require('../column/columnid');

/// convert Smartsheet rows into JSON

/// cellObj example

let parseCells2Data = (row, colIdNamePairs) => {
    let cellsData = {};
    for (let cellObj of row.cells) {
        let key = Object.keys(colIdNamePairs)
            .find(key => colIdNamePairs[key] === cellObj.columnId);

        if (cellObj.hyperlink && cellObj.hyperlink.url) { // extract url if cell has a hyperlink
            let title = cellObj.value;

            // give it a generic 'Link' title if none was set in smartsheet
            if (title === cellObj.hyperlink.url) {
                title = 'Link';
            }

            cellsData[key] = { title: title, url: cellObj.hyperlink.url };
        } else {
            cellsData[key] = cellObj.value;
        }
    }
    return cellsData;
};

let parseRows2Data = async (ssClient, sheetId, rowIds = []) => {
    let colIdNamePairs = await columnid.listColumnIds(ssClient, sheetId);
    let rowsData = [];
    for (let id of rowIds) {
        let row = await ssClient.sheets.getRow({
            sheetId: sheetId,
            rowId: id
        });
        rowsData.push(parseCells2Data(row, colIdNamePairs));
    }
    return rowsData;
};

/// construct cell value object
/// need to handle the hyperlink val: {"value": 8113, "url":"http://..."}
let constrCellObj = (columnid, val) => {
    let obj = {columnId: columnid};
    if (val && typeof val == 'object' && val.url) {
        obj['value'] = (val.value || 'Link').toString();
        obj['hyperlink'] = {
            "url": val.url
        }
    } else {
        obj['value'] = val;
    }
    return obj;
};
/// convert Oracle format data into Smartsheet format
let parseData2Cells = (data, colIdNamePairs) => {
    let cells = [];
    for (let [name, id] of Object.entries(colIdNamePairs)) {
        if (data.hasOwnProperty(name)) {
            cells.push(constrCellObj(id, data[name]));
        }
    }
    return cells;
}

/**
 * Parses a single Oracle Db row into a Smartsheet row
 * @param {*} data Oracle Db row data
 * @param {unknown} colIdNamePairs
 * @returns {SSRow} An {@link SSRow} object representing the Oracle Db row
 */
let parseData2Row = (data, colIdNamePairs) => {
    let cellsData = parseData2Cells(data, colIdNamePairs);
    return {
        toTop: true,
        cells: cellsData
    };
};

/**
 * Parses Oracle Db data into Smartsheet rows
 *
 * Sample output:
 * ```
 * [
 *   {
 *     toTop: true,
 *     cells: [
 *       { columnId: 2768462975133572, value: 'PARK: Hathaway Park Renovation' },
 *       { columnId: 7272062602504068, value: 8113 },
 *       { columnId: 1642563068290948, value: 'Mastrodicasa, Chris' },
 *       { columnId: 6146162695661444, value: 'Phase V - Construction' },
 *       { columnId: 3894362881976196, value: 0 },
 *       { columnId: 8397962509346692, value: 0 },
 *       { columnId: 1079613114869636, value: 0 },
 *       { columnId: 5583212742240132, value: 'Agbayani Construction Corporation' },
 *       { columnId: 3331412928554884, value: '2020-05-07T16:53:00.000Z' },
 *       { columnId: 7835012555925380, value: 1168793.03 },
 *       { columnId: 2205513021712260, value: 1177105 },
 *       { columnId: 6709112649082756, value: 8311.97 }
 *     ]
 *   },
 *   ...
 * ]
 * ```
 * @param ssClient Smartsheet client
 * @param sheetId Sheet Id to map columns to
 * @param dataArray Oracle Db data
 * @returns {Promise<SSRow[]>} A promise that resolves to an array of {@link SSRow} objects
 */
let parseData2Rows = async (ssClient, sheetId, dataArray) => {
    let colIdNamePairs = await columnid.listColumnIds(ssClient, sheetId); // todo: what happens if this errors?
    return dataArray.map((data) => {
        return parseData2Row(data, colIdNamePairs);
    });
};

/**
 * Inserts rows from Oracle Db to Smartsheet
 *
 * dataArray sample:
 * ```
 * [
 *   {
 *     "PRJ_NAME":"PARK: Hathaway Park Renovation",
 *     "PRJ_ID":8113,
 *     "IMPL_MGR":"Mastrodicasa, Chris",
 *     "PRJ_PHASE":"Phase V - Construction",
 *     "ARCHIVED":0,
 *     "SWPPP":0,
 *     "C3":0,
 *     "CONTRACTOR":"Agbayani Construction Corporation",
 *     "LAST_UPDATE":"2020-05-07T16:53:00.000Z",
 *     "CURR_EXPENDITURE":1168793.03,
 *     "CURRENT_TOTAL_BUDGET":1177105,
 *     "CURR_RM_BALANCE":8311.97
 *   },
 *   ...
 * ]
 * ```
 * @param {*} ssClient Smartsheet client
 * @param {number} sheetId Sheet Id to insert rows to
 * @param {*[]} dataArray Array of data to insert from Oracle Db
 * @returns {Promise<AddRowsResponse | SSError>} A promise that resolves to an {@link AddRowsResponse}
 * or to a generic {@link SSError}
 */
let insertRows = async (ssClient, sheetId, dataArray) => {
    /** @type (AddRowsResponse | SSError) */
    let res;
    let ssRows = await parseData2Rows(ssClient, sheetId, dataArray);
    let opts = {
        sheetId: sheetId,
        body: ssRows
    };
    try {
        res = await ssClient.sheets.addRows(opts);
    } catch (e) { return e; }
    return res;
};

/**
 * Deletes a set of rows from a given sheet
 * @param {*} ssClient Smartsheet client
 * @param {number} sheetId Sheet Id to delete rows from
 * @param {number[]} rowIds Array of row ids to delete
 * @returns {Promise<void | DeleteRowsResponse | SSError>}
 */
let deleteRows = async (ssClient, sheetId, rowIds) => {
    /** @type (DeleteRowsResponse | SSError) */
    let res;
    if (!rowIds || rowIds.length === 0) { return; }
    let opts = {
        sheetId: sheetId,
        rowId: rowIds
    };
    try {
        res = await ssClient.sheets.deleteRow(opts);
    } catch (e) { return e; }
    return res;
};

module.exports = {
    parseRows2Data: parseRows2Data,
    parseCells2Data: parseCells2Data,
    parseData2Rows: parseData2Rows,
    insertRows: insertRows,
    deleteRows: deleteRows
};