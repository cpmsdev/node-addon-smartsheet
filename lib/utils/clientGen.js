'use strict';

let client = require('smartsheet');

let getSmartsheetClient = (smartsheetOpts) => {
    let smartsheetClient = client.createClient(smartsheetOpts);
    return smartsheetClient;
};

module.exports = {
    getClient: getSmartsheetClient
}