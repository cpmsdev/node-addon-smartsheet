'use strict';

// data sample //
//[
//     {
//       "id": "AAAAATYU54QAD7_fNhTnhA",
//       "name": "David Davidson",
//       "email": "dd@example.com"
//     },
//     {
//       "id": "AAXFBiYU54QAEeWu5hTnhA",
//       "name": "Ed Edwin",
//       "email": "ee@example.com"
//     }
//   ]
// output //
// {
//      "Ed Edwin": "ee@example.com",
//      "David Davidson": "dd@example.com"
// }
let generateKeyValPairs = (dataArray, keyProp, valProp) =>{
    let dataPairs = {};
    for (let data of dataArray) {
        dataPairs[data[keyProp]] = data[valProp];
    }
    return dataPairs;
};

module.exports = {
    genKeyValPairs: generateKeyValPairs
};