/**
 * @readonly
 * @enum {string}
 */
const SSAccessLevel = Object.freeze({
    ADMIN: 'ADMIN',
    COMMENTER: 'COMMENTER',
    EDITOR: 'EDITOR',
    EDITOR_SHARE: 'EDITOR_SHARE',
    OWNER: 'OWNER',
    VIEWER: 'VIEWER'
});

/**
 * @readonly
 * @enum {string}
 */
const SSObjectType = Object.freeze({
    ABSTRACT_DATETIME: 'ABSTRACT_DATETIME',
    CONTACT: 'CONTACT',
    DATE: 'DATE',
    DATETIME: 'DATETIME',
    DURATION: 'DURATION',
    MULTI_CONTACT: 'MULTI_CONTACT',
    MULTI_PICKLIST: 'MULTI_PICKLIST',
    PREDECESSOR_LIST: 'PREDECESSOR_LIST'
});

/**
 * @readonly
 * @enum {string}
 */
const SSTag = Object.freeze({
    CALENDAR_END_DATE: 'CALENDAR_END_DATE',
    CALENDAR_START_DATE: 'CALENDAR_START_DATE',
    CARD_DONE: 'CARD_DONE',
    GANTT_ALLOCATION: 'GANTT_ALLOCATION',
    GANTT_ASSIGNED_RESOURCE: 'GANTT_ASSIGNED_RESOURCE',
    GANTT_DISPLAY_LABEL: 'GANTT_DISPLAY_LABEL',
    GANTT_DURATION: 'GANTT_DURATION',
    GANTT_END_DATE: 'GANTT_END_DATE',
    GANTT_PERCENT_COMPLETE: 'GANTT_PERCENT_COMPLETE',
    GANTT_PREDECESSOR: 'GANTT_PREDECESSOR',
    GANTT_START_DATE: 'GANTT_START_DATE',
    BASELINE_START_DATE: 'BASELINE_START_DATE',
    BASELINE_END_DATE: 'BASELINE_END_DATE',
    BASELINE_VARIANCE: 'BASELINE_VARIANCE'
});

/**
 * @readonly
 * @enum {string}
 */
const SSColumnType = Object.freeze({
    // todo:
});

/**
 * @typedef {Object} SSError
 * @property {string} refId
 * @property {number} errorCode
 * @property {string} message
 * @property {{ index: number }} [detail]
 */

/**
 * @typedef {Object} SSProjectSettings
 * @property {number} lengthOfDay [1 .. 24]
 * @property {string[]} nonWorkingDays
 * @property {('MONDAY' | 'TUESDAY' | 'WEDNESDAY' | 'THURSDAY' | 'FRIDAY' | 'SATURDAY' | 'SUNDAY')[]} workingDays
 */

/**
 * @typedef {Object} SSUserPermissions
 * @property {('ADMIN' | 'READ_DELETE' | 'READ_ONLY' | 'READ_WRITE')} summaryPermissions
 */

/**
 * @typedef {Object} SSUserSettings
 * @property {boolean} criticalPathEnabled
 * @property {boolean} displaySummaryTasks
 */

/**
 * @typedef {Object} SSSource
 * @property {number} id
 * @property {('report' | 'sheet' | 'sight' | 'template')} type
 */

/**
 * @typedef {Object} SSHyperlink
 * @property {number} [reportId]
 * @property {number} [sheetId]
 * @property {number} [sightId]
 * @property {string} [url]
 */

/**
 * @typedef {Object} SSCellLink
 * @property {number} columnId
 * @property {number} rowId
 * @property {number} sheetId
 * @property {string} sheetName
 * @property {('BLOCKED' | 'BROKEN' | 'CIRCULAR' | 'DISABLED' | 'INACCESSIBLE' | 'INVALID' | 'NOT_SHARED' | 'OK')} status
 */

/**
 * @typedef SSObjectValue
 * @property {SSObjectType} objectType
 */

/**
 * @typedef {Object} SSCell
 * @property {number} [columnId]
 * @property {string} [columnType]
 * @property {string} [conditionalFormat]
 * @property {string} [displayValue]
 * @property {string} [format]
 * @property {string} [formula]
 * @property {SSHyperlink} [hyperlink]
 * @property {SSCellImage} [image]
 * @property {SSCellLink} [linkInFromCell]
 * @property {SSCellLink[]} [linksOutToCells]
 * @property {SSObjectValue} [objectValue]
 * @property {boolean} [overrideValidation]
 * @property {boolean} [strict]
 * @property {string | number | boolean} [value]
 */

/**
 * @typedef {Object} SSColumn
 * @property {Object} [autoNumberFormat]
 * @property {SSContactOption[]} [contactOptions]
 * @property {string} [description]
 * @property {string} [format]
 * @property {string} [formula]
 * @property {boolean} [hidden]
 * @property {number} [id]
 * @property {number} [index]
 * @property {boolean} [locked]
 * @property {boolean} [lockedForUser]
 * @property {string[]} [options]
 * @property {boolean} [primary]
 * @property {string} [symbol]
 * @property {('AUTO_NUMBER' | 'CREATED_BY' | 'CREATED_DATE' | 'MODIFIED_BY' | 'MODIFIED_DATE')} [systemColumnType]
 * @property {SSTag[]} [tags]
 * @property {string} [title]
 * @property {SSColumnType} [type]
 * @property {boolean} [validation]
 * @property {0 | 1 | 2} [version]
 * @property {number} [width]
 */

/**
 * @typedef {Object} SSRow
 * @property {number} [id]
 * @property {number} [sheetId]
 * @property {SSAccessLevel} [accessLevel]
 * @property {SSAttachment[]} [attachments]
 * @property {SSCell[]} [cells]
 * @property {SSColumn[]} [columns]
 * @property {string} [conditionalFormat]
 * @property {string | number} [createdAt]
 * @property {SSDiscussion[]} [discussions]
 * @property {SSProof} [proofs]
 * @property {boolean} [expanded]
 * @property {boolean} [filteredOut]
 * @property {string} [format]
 * @property {boolean} [inCriticalPath]
 * @property {boolean} [locked]
 * @property {boolean} [lockedForUser]
 * @property {string | number} [modifiedAt]
 * @property {{ email: string, name: string }} [modifiedBy]
 * @property {string} [permalink]
 * @property {number} [rowNumber] \>= 1
 * @property {boolean} [toTop]
 * @property {boolean} [toBottom]
 * @property {number} [parentId]
 * @property {number} [siblingId]
 * @property {boolean} [above]
 * @property {number} [indent] Must be 1
 * @property {number} [outdent] Must be 1
 * @property {number} [version]
 */

/**
 * @typedef {Object} SSSheet
 * @property {number} [id]
 * @property {number} [fromId]
 * @property {number} [ownerId]
 * @property {SSAccessLevel} [accessLevel]
 * @property {SSAttachment[]} [attachments]
 * @property {SSColumn[]} [columns]
 * @property {string | number} [createdAt]
 * @property {SSCrossSheetRef[]} [crossSheetReferences]
 * @property {boolean} [dependenciesEnabled]
 * @property {SSDiscussion[]} [discussions]
 * @property {string[]} [effectiveAttachmentOptions]
 * @property {boolean} [favorite]
 * @property {boolean} [ganttEnabled]
 * @property {boolean} [hasSummaryFields]
 * @property {string | number} [modifiedAt]
 * @property {string} [name]
 * @property {string} [owner]
 * @property {string} [permalink]
 * @property {SSProjectSettings} [projectSettings]
 * @property {boolean} [readOnly]
 * @property {boolean} [resourceManagementEnabled]
 * @property {('NONE' | 'LEGACY_RESOURCE_MANAGEMENT' | 'RESOURCE_MANAGEMENT_BY_SMARTSHEET')} [resourceManagementType]
 * @property {SSRow[]} [rows]
 * @property {boolean} [showParentRowsForFilters]
 * @property {SSSource} [source]
 * @property {Object} [summary]
 * @property {number} [totalRowCount]
 * @property {SSUserPermissions} [userPermissions]
 * @property {SSUserSettings} [userSettings]
 * @property {number} [version]
 * @property {Object} [workspace]
 */

/**
 * @typedef {Object[]} ListSheetResponse
 * @property {number} pageNumber
 * @property {number} [pageSize]
 * @property {number} totalPages
 * @property {number} totalCount
 * @property {SSSheet[]} data
 */

/**
 * @typedef {Object} DeleteSheetResponse
 * @property {('PARTIAL_SUCCESS' | 'SUCCESS')} message
 * @property {0 | 3} resultCode
 */

/**
 * @typedef {Object} CreateSheetResponse
 * @property {('PARTIAL_SUCCESS' | 'SUCCESS')} message
 * @property {0 | 3} resultCode
 * @property {SSSheet} result
 */

/**
 * @typedef {Object} AddRowsResponse
 * @property {('PARTIAL_SUCCESS' | 'SUCCESS')} message
 * @property {0 | 3} resultCode
 * @property {SSRow[]} result
 */

/**
 * @typedef {Object} DeleteRowsResponse
 * @property {('PARTIAL_SUCCESS' | 'SUCCESS')} message
 * @property {0 | 3} resultCode
 * @property {number[]} result
 */