'use strict';

let expect = require("chai").expect;
let tempData = require('./global/tempData');
let FixturesCls = require('./global/fixtures');
let fixture = new FixturesCls();
let ssClient = fixture.ssClient;

before(async () => {
    await fixture.initWorkspace();
});

describe('Webhook Functionalities', async () => {
    let tempSheet,
        sheetDef = tempData.sheetBody.oracle;
    before(async () => {
        tempSheet = await fixture.createTempSheet(sheetDef);
    });

    after(async () => {
        await fixture.deleteTempSheet(tempSheet);
    });

    describe('Webhook Load/Clean/List Functions', async () => {
        let webhook = require('../lib/hook/webhook');

        it('can reload and enable webhooks using an array of webhook configs', async () => {
            let hookConfigs = [{
                name: "Webhook Test #1",
                callbackUrl: "https://dev.test.net/sscallback",
                scopeObjectId: tempSheet.id,
                subscope: {
                    "columnIds": [tempSheet.columns[0].id, tempSheet.columns[1].id]
                }
            }, {
                name: "Webhook Test #2",
                callbackUrl: "https://dev.test.net/callback",
                scopeObjectId: tempSheet.id
            }];
            let hooksRes = await webhook.reloadWebhooks(ssClient, hookConfigs);
            expect(hooksRes).to.be.an('array');
            for (let i in hooksRes) {
                expect(hooksRes[i]).to.deep.include({message: 'SUCCESS'});
                expect(hooksRes[i]).to.nested.include({'result.name': hookConfigs[i].name});
                expect(hooksRes[i]).to.nested.include({'result.status': 'ENABLED'});
            }
        });

        it('can list all webhooks', async () => {
            let hookList = await webhook.listWebhooks(ssClient);
            expect(hookList).to.be.an('array');
            for (let h of hookList) {
                expect(h).to.have.all.keys(  "callbackUrl", "createdAt", "enabled",
                "events", "id", "modifiedAt", "name", "scope", "scopeObjectId", "sharedSecret",
                "status", "subscope", "version");
            }
        });

        it('can remove all webhooks', async () => {
            let deletedRes = await webhook.cleanWebhooks(ssClient);
            expect(deletedRes).to.be.an('array');
            for (let d of deletedRes) {
                expect(d).to.deep.include({message: 'SUCCESS'});
            }
        });
    });

});