'use strict';

let expect  = require("chai").expect;
let tempData = require('./global/tempData');
let FixturesCls = require('./global/fixtures');
let fixture = new FixturesCls();
let ssClient = fixture.ssClient;

before(async () => {
    await fixture.initWorkspace();
});

describe('Sheet Functionalities', () => {

    describe('Sheet Basic Functionalities', async () => {
        let sheet = require('../lib/sheet/sheet');
        let sheetDef = tempData.sheetBody.basic;

        it('can create a new sheet in a workspace', async () => {
            let sheetObj = await sheet.createSheet(ssClient, sheetDef.name, sheetDef.columns, fixture.tempWs.id);
            expect(sheetObj).is.not.empty;
            expect(sheetObj.id).to.be.a('number').to.be.above(Math.pow(10,10));
        });

        it('can check if a sheet exists', async () => {
            let thisSheet = await sheet.getSheetByName(ssClient, sheetDef.name);
            expect(thisSheet).to.be.a('object').to.have.property('name').to.equal(sheetDef.name);
        });

        it('can delete a sheet', async () => {
            let thisSheet = await sheet.getSheetByName(ssClient, sheetDef.name);
            let deletedRes = await sheet.deleteSheet(ssClient, thisSheet.id);
            expect(deletedRes).has.property('message').to.equal('SUCCESS');
        });
    });

    describe('Sheet Data Functionalities', async () => {
        let sheetdata = require('../lib/sheet/sheetdata');
        let tempSheet, sheetDef = tempData.sheetBody.oracle;
        before(async () => {
            tempSheet = await fixture.createTempSheet(sheetDef);
        });

        after(async () => {
            await fixture.deleteTempSheet(tempSheet);
        });

        it('can insert data to a sheet', async () => {
            let insertedRes = await sheetdata.insertData(ssClient, tempSheet.id, tempData.oraData);
            expect(insertedRes).has.property('message').to.equal('SUCCESS');
        });

        it('can log comment with timestamp', async () => {
            let logRes = await sheetdata.log(ssClient, tempSheet.id, 'Test Comment');
            expect(logRes).has.property('message').to.equal('SUCCESS');
        });

        it('can parse the entire sheet data', async () => {
            let data = await sheetdata.parseAllData(ssClient, tempSheet.id);
            expect(data).to.be.an('array').to.have.lengthOf(tempData.oraData.length);
            expect(data[0]).to.have.all.keys(Object.keys(tempData.oraData[0]));
        });


        it('can parse large-size sheet data', async () => {
            let data = await sheetdata.parseAllData(ssClient, fixture.tempSheet.id);
            expect(data).to.be.an('array').to.have.lengthOf(fixture.tempSheet.numOfRows);
        });


        it('can delete all data in a sheet', async () => {
            let deletedRes = await sheetdata.deleteAllData(ssClient, tempSheet.id);
            expect(deletedRes).has.property('message').to.equal('SUCCESS');
        });

    });

});