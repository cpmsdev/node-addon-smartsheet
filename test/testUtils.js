'use strict';

let expect  = require("chai").expect;


describe('Common-used Utilities', () => {

    describe('Key Value Pairs', () => {
        let keyValPairs = require('../lib/utils/keyvalPairs');
        it('can generate a key/value pairs object', () => {
            let data = [
                {
                    "id": "AAAAATYU54QAD7_fNhTnhA",
                    "name": "David Davidson",
                    "email": "dd@example.com"
                },
                {
                    "id": "AAXFBiYU54QAEeWu5hTnhA",
                    "name": "Ed Edwin",
                    "email": "ee@example.com"
                }
            ];
            let pairs = keyValPairs.genKeyValPairs(data, 'name', 'email');
            expect(pairs).to.have.all.keys(['David Davidson', 'Ed Edwin']);
        });

    });
});