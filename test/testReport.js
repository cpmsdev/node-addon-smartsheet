'use strict';

let expect  = require("chai").expect;
let FixturesCls = require('./global/fixtures');
let fixture = new FixturesCls();
let ssClient = fixture.ssClient;

before(async () => {
    await fixture.initWorkspace();
});

describe('Report Functionalities', () => {
    let report = require('../lib/report/reportdata');

    describe('Report Data Functionalities', async () => {
        it('can parse the entire report data', async () => {
            let data = await report.parseReportData(ssClient, fixture.tempRpt.id);
            expect(data).to.be.an('array').to.have.lengthOf(fixture.tempRpt.numOfRows);
        });
    });
});