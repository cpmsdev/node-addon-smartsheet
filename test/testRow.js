'use strict';

let expect = require("chai").expect;
let tempData = require('./global/tempData');
let FixturesCls = require('./global/fixtures');
let fixture = new FixturesCls();
let ssClient = fixture.ssClient;

before(async () => {
    await fixture.initWorkspace();
});

describe('Row Functionalities', async () => {
    let tempSheet, tempRowIds,
        sheetDef = tempData.sheetBody.oracle;
    before(async () => {
        tempSheet = await fixture.createTempSheet(sheetDef);
    });

    after(async () => {
        await fixture.deleteTempSheet(tempSheet);
    });

    describe('Row Data Functions', async () => {
        let rowdata = require('../lib/row/rowdata');

        it('can translate Oracle data into Smartsheet rows', async () => {
            let ssData = await rowdata.parseData2Rows(ssClient, tempSheet.id, tempData.oraData);
            expect(ssData).to.be.an('array');
            for (let d of ssData) {
                expect(d).to.have.nested.property('cells[0].columnId');
            }
        });

        it('can insert rows into a sheet', async () => {
            let insertedRes = await rowdata.insertRows(ssClient, tempSheet.id, tempData.oraData);
            expect(insertedRes).to.deep.include({message: 'SUCCESS'});
            expect(insertedRes.result).to.be.an('array').to.have.lengthOf(tempData.oraData.length);
            for (let r of insertedRes.result) {
                expect(r).to.have.property(  'cells');
            }
            tempRowIds = insertedRes.result.map(obj => obj.id);
        });

        it('can convert Smartsheet rows into a data array ', async () => {
            let dataArray = await rowdata.parseRows2Data(ssClient, tempSheet.id, tempRowIds);
            expect(dataArray).to.be.an('array').to.have.lengthOf(tempData.oraData.length);
            for (let d of dataArray) {
                expect(d).to.have.all.keys(tempData.oraData[0]);
            }
        });

        it('can delete rows from a sheet', async () => {
            let deletedRes = await rowdata.deleteRows(ssClient, tempSheet.id, tempRowIds);
            expect(deletedRes).to.deep.include({message: 'SUCCESS'});
            expect(deletedRes.result).to.be.an('array').to.have.lengthOf(tempData.oraData.length);
        });

    });

});