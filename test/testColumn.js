'use strict';

let expect  = require("chai").expect;
let tempData = require('./global/tempData');
let FixturesCls = require('./global/fixtures');
let fixture = new FixturesCls();
let ssClient = fixture.ssClient;

before(async () => {
    await fixture.initWorkspace();
});

describe('Column Functionalities', async () => {
    let tempDataSheet, tempSheet,
        sheetDef = tempData.sheetBody.basic, /// a empty sheet
        dataSheetDef = tempData.sheetBody.oracle; /// a sheet with data
    let tempDataColumnId, tempPicklistColumnId;

    before(async () => {
        let sheetdata = require('../lib/sheet/sheetdata');
        /// create a data sheet
        tempDataSheet = await fixture.createTempSheet(dataSheetDef);
        tempDataColumnId = tempDataSheet.columns.find(obj => obj.type === 'TEXT_NUMBER').id;
        await sheetdata.insertData(ssClient, tempDataSheet.id, tempData.oraData);

        /// create a sheet with a pick list column
        tempSheet = await fixture.createTempSheet(sheetDef);
        tempPicklistColumnId = tempSheet.columns.find(obj => obj.type === 'PICKLIST').id;
    });

    after(async () => {
        await fixture.deleteTempSheet(tempDataSheet);
        await fixture.deleteTempSheet(tempSheet);
    });

    describe('Column Basic Functionalities', async () => {
        let column = require('../lib/column/column');

        it('can construct columns with Oracle data', async () => {
            let columnDefs = column.defColsFromData(tempData.oraData, 'PRJ_ID');
            expect(columnDefs).to.be.an('array').to.have.lengthOf(8);
            expect(columnDefs.filter(obj => obj.type === 'DATE')).to.have.lengthOf(1);
            expect(columnDefs.filter(obj => obj.type === 'TEXT_NUMBER')).to.have.lengthOf(7);
        });

        it('can read a column into an array of column values', async () => {
            let valArray = await column.getColumnData(ssClient, tempDataSheet.id, tempDataColumnId);
            expect(valArray).is.an('array').to.have.lengthOf(3);
            expect(valArray[0]).is.not.a('object');
        });

        it('can find a column in a sheet by column name', async () => {
            let columnObj = await column.getColumnByName(ssClient, tempDataSheet.id, 'PRJ_ID');
            expect(columnObj).to.be.an('object');
            expect(columnObj.id).to.be.a('number').to.be.above(Math.pow(10,10));
        });
    });

    describe('Column ID Functions', async () => {
        let columnId = require('../lib/column/columnid');

        it('can list all columns with IDs', async () => {
            let nameIdPairs = await columnId.listColumnIds(ssClient, tempSheet.id);
            expect(nameIdPairs).to.be.a('object').have.property(sheetDef.columns[0].title);
        });

        it('can handle wrong access token', async () => {
            let nameIdPairs = await columnId.listColumnIds(fixture.ssClientBad, tempSheet.id);
            expect(nameIdPairs).to.be.a('object').have.property('errorCode');
        });
    });

    describe('Picklist Functionalities', async () => {
        let picklist = require('../lib/column/picklist');

        it('can sync a picklist data with another column data', async () => {
            let sourceColumn = {
                sheetId: tempDataSheet.id,
                columnId: tempDataColumnId
            };
            let destinationColumn = {
                sheetId: tempSheet.id,
                columnId: tempPicklistColumnId
            };
            let updated = await picklist.syncPicklist(ssClient, sourceColumn, destinationColumn);
            expect(updated.message).to.equal('SUCCESS');
            expect(updated.result).have.property('options').to.have.lengthOf(3);
        });

        it('can update a picklist data with given data array', async () => {
            let data = [
                '2020-10-23 10:30AM Zoom Meeting',
                '2020-11-13 10:30AM Zoom Meeting',
                '2020-12-11 10:30AM Zoom Meeting',
                '2021-01-15 10:30AM Zoom Meeting'
            ];
            let destinationColumn = {
                sheetId: tempSheet.id,
                columnId: tempPicklistColumnId
            };
            let updated = await picklist.updatePicklist(ssClient, data, destinationColumn);
            expect(updated.message).to.equal('SUCCESS');
            expect(updated.result).have.property('options').to.have.lengthOf(4);
        });

    });
});