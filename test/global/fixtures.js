'use strict';

let tempData = require('./tempData');

class Fixtures {
    ssClient;
    ssClientBad;
    tempRpt;
    tempWs;
    tempSheet;

    constructor() {
        this.initSsClients();
    }

    initSsClients() {
        let client = require('smartsheet');
        this.ssClient = client.createClient(require('../../config').smartsheetOpts);
        this.ssClientBad = client.createClient(require('../../config').badSmartsheetOpts);
        this.tempRpt = require('../../config').testConfig.report;
        this.tempSheet = require('../../config').testConfig.sheet;
    }

    async initWorkspace() {
        let thisWs;
        try {
            let wsList = await this.ssClient.workspaces.listWorkspaces();
            if (wsList.totalCount > 0) {
                thisWs = wsList.data.find(ws => (ws.name === tempData.wsBody.name));
            }
            if (!thisWs) {
                let res = await this.ssClient.workspaces.createWorkspace({
                    body: tempData.wsBody
                });
                thisWs = res.result;
            }
        } catch (e) {
            console.error(e);
        }
        return this.tempWs = thisWs;
    }

    async createTempSheet(sheetBody) {
        let thisSheet;
        try {
            thisSheet = await this.ssClient.sheets.createSheetInWorkspace({
                workspaceId: this.tempWs.id,
                body: sheetBody
            });
        } catch (e) {
            console.error(e);
        }
        console.log(`*** Created sheet ${thisSheet.result.id} ***`);
        return thisSheet.result;
    }

    async deleteTempSheet(sheet) {
        let delRes
        try {
            delRes = await this.ssClient.sheets.deleteSheet({id: sheet.id});
        } catch (e) {
            return console.error(e);
        }
        console.log(`*** Deleted sheet ${sheet.id} - ${delRes.message} ***`);
        return delRes;
    }

}


module.exports = Fixtures;


