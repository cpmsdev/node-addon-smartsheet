'use strict';

let workspace =  {
    name: '[TEMP] TESTING WORKSPACE'
};

let oracleData = [{"PRJ_NAME":"PARK: Hathaway Park Renovation",
        "PRJ_ID":8113,
        "PRJ_LINK": {"value": "8113", "url":"http://127.0.0.1:3001/cip/prj/8113"},
        "IMPL_MGR":"Mastrodicasa, Chris",
        "PRJ_PHASE":"Phase V - Construction",
        "ARCHIVED":0,
        "CONTRACTOR":"Agbayani Construction Corporation",
        "LAST_UPDATE":"2020-05-07T16:53:00.000Z",
        "CURR_EXPENDITURE":1168793.03},
    {"PRJ_NAME":"PARK: Watson Park Renovations",
        "PRJ_ID":7538,
        "PRJ_LINK": {"value": 7538, "url":"http://127.0.0.1:3001/cip/prj/7538"},
        "IMPL_MGR":"Lindores, Vanessa",
        "PRJ_PHASE":"Phase V - Construction",
        "ARCHIVED":0,
        "CONTRACTOR":"Suarez & Munoz Construction, Inc.",
        "LAST_UPDATE":"2020-05-10T16:53:00.000Z",
        "CURR_EXPENDITURE":2466977.17},
    {"PRJ_NAME":"TRAIL: Three Creeks Pedestrian Bridge over Los Gatos Crk",
        "PRJ_ID":7232,
        "PRJ_LINK": {"value": 7232, "url":"http://127.0.0.1:3001/cip/prj/7232"},
        "IMPL_MGR":"Yano, Yoshifumi",
        "PRJ_PHASE":"Phase V - Construction",
        "ARCHIVED":0,
        "CONTRACTOR":"Gordon N. Ball, Inc.",
        "LAST_UPDATE":"2019-05-07T16:53:00.000Z",
        "CURR_EXPENDITURE":1545899.79}];

let basicEmptySheet = {
    name: '[TEMP] BASIC TESTING SHEET',
    columns: [
        {
            "title": "Favorite",
            "type": "CHECKBOX",
            "symbol": "STAR"
        }, {
            "title": "Project ID",
            "primary": true,
            "type": "TEXT_NUMBER"
        }, {
            "title": "Pick List",
            "type": "PICKLIST",
            "options": [
                "option 1",
                "option 2"
            ]
        }
    ]
};

let oracleSheet = {
    name: '[TEMP] ORACLE TESTING SHEET',
    columns: [{"title":"PRJ_NAME","type":"TEXT_NUMBER"},
        {"title":"PRJ_ID","type":"TEXT_NUMBER","primary":true},
        {"title":"PRJ_LINK","type":"TEXT_NUMBER"},
        {"title":"IMPL_MGR","type":"TEXT_NUMBER"},
        {"title":"PRJ_PHASE","type":"TEXT_NUMBER"},
        {"title":"ARCHIVED","type":"TEXT_NUMBER"},
        {"title":"CONTRACTOR","type":"TEXT_NUMBER"},
        {"title":"LAST_UPDATE","type":"DATE"},
        {"title":"CURR_EXPENDITURE","type":"TEXT_NUMBER"}]

};

let dropdown = {
    data: {
        sheetId: '',
        columnId: ''
    },
    target: {
        sheetId: '',
        columnId: ''
    }
};

module.exports = {
    oraData: oracleData,
    wsBody: workspace,
    sheetBody: {
        basic: basicEmptySheet,
        oracle: oracleSheet
    },
    dropdown: dropdown
};