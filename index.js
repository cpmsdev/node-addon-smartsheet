'use strict';

let utilClientGen = require('./lib/utils/clientGen');
let sheet = require('./lib/sheet/sheet');
let sheetData = require('./lib/sheet/sheetdata');
let reportData = require('./lib/report/reportdata');
let rowData = require('./lib/row/rowdata');
let column = require('./lib/column/column');
let columnId = require('./lib/column/columnid');
let picklist = require('./lib/column/picklist');
let webhook = require('./lib/hook/webhook');
let webhookCb = require('./lib/hook/callback');

// Smartsheet error message sample
// {
//     statusCode: 401,
//     errorCode: 1002,
//     message: "Your Access Token is invalid."
// }

module.exports = {
    createClient: utilClientGen.getClient, /// create a Smartsheet client
    sheet: {
        /// Create a new sheet
        /// input parameters:
        /// ssClient: Smartsheet client
        /// sheetName: name of the new sheet
        /// columns: column objects (see example in tempData.js)
        /// workspaceId
        /// returned value: sheet object
        create: sheet.createSheet,
        /// Delete a sheet
        /// input parameters: ssClient, sheetId
        /// returned value: success message object
        delete: sheet.deleteSheet,
        /// Retrieve a sheet by sheet's name
        /// input parameters: ssClient, sheetName
        /// returned value: sheet object
        getSheetByName: sheet.getSheetByName,
        /// Load data to a sheet
        /// input parameters: ssClient, sheetId, dataArray (see data format in tempData.js)
        /// returned value: success message object
        loadData: sheetData.insertData,
        /// Delete all data in a sheet
        /// input parameters: ssClient, sheetId
        /// returned value: success message object
        deleteAllData: sheetData.deleteAllData,
        /// Convert all data into a key:value dataArray (see data format in tempData.js)
        /// input parameters: ssClient, sheetId
        /// returned value: dataArray
        parseAllData: sheetData.parseAllData,
        /// Log comment with timestamp in a sheet
        /// input parameters: ssClient, sheetId, text
        /// returned value: success message object
        log: sheetData.log
    },
    row: {
        /// Convert Smartsheet row objects to Oracle data array
        /// input parameters: ssClient, sheetId, rowIds
        /// returned value: data array (see Oracle data format in tempData.js)
        parseRows2Data: rowData.parseRows2Data
    },
    column: {
        /// Parse Oracle query data array to get column objects
        /// input parameters: ssClient, dataArray
        /// returned value: column objects
        genColumnsFromData: column.defColsFromData,
        /// Get a list of Column Name & Column ID pairs
        /// input parameters: ssClient, sheetId
        /// returned value: <object> a list of Column Name:ID pairs
        listColumnIds: columnId.listColumnIds,
        /// Parse Oracle query data array to get column objects
        /// input parameters: ssClient, sheetId, columnName
        /// returned value: column object
        getColumnByName: column.getColumnByName,
        /// Read the entire column's data in a sheet
        /// input parameters: ssClient, sheetId, columnId
        /// returned value: an array of column values
        getColumnData: column.getColumnData,
        /// Update picklist options for a PICKLIST type column using data from another column
        /// input parameters:
        /// ssClient
        /// sourColumn: {sheetID: ***, columnId: ***}
        /// destColumn: {sheetID: ***, columnId: ***}
        /// returned value: success message object
        syncPicklist: picklist.syncPicklist,
        /// Update picklist options for a PICKLIST type column using given data array
        /// input parameters: ssClient, dataArray, destColumn
        /// returned value: success message object
        updatePicklist: picklist.updatePicklist
    },
    report: {
        /// Convert all data into a key:value dataArray (see data format in tempData.js)
        /// input parameters: ssClient, reportId
        /// returned value: dataArray
        parseAllData: reportData.parseReportData
    },
    webhook: {
        /// Refresh all webhooks defined in configuration
        /// input parameters: ssClient, hookConfigs (see testWebhook.js for config examples)
        /// returned value: success message object
        reloadWebhooks: webhook.reloadWebhooks,
        /// Remove all existing webhooks
        /// input parameters: ssClient
        /// returned value: success message object
        removeAllWebhooks: webhook.cleanWebhooks,
        /// List all existing webhooks
        /// input parameters: ssClient
        /// returned value: a list of webhook objects
        listWebhooks: webhook.listWebhooks,
        /// Process webhook callback body data
        /// input parameters: ssClient
        /// returned value:
        /// 1) callback validation object {smartsheetHookResponse: body.challenge}
        /// 2) an array of sheet/rows/cells update events
        /// 3) webhook status change message {message: ''}
        /// 4) other events {message: ''}
        processWebhookBody: webhookCb.processBody
    }
};